<?php

/**
 * Filter handler to specify whether a taxonomy terms is top level or not.
 */
class views_taxonomy_hierarchy_top_filter_handler_filter extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();

    $where = "$this->table_alias.$this->real_field ";

    if (!empty($this->value)) {
      $where .= '= 0';
    }
    else {
      $where .= '<> 0';
    }
    $this->query->add_where($this->options['group'], $where);
  }
}
