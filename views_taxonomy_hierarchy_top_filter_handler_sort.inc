<?php
class views_taxonomy_hierarchy_top_filter_handler_sort extends views_handler_sort {
  function query() {
    $this->ensure_my_table();

    $formula = "({$this->table_alias}.{$this->real_field} <> 0)";
    $this->query->add_orderby(NULL, $formula, $this->options['order'], '_' . $this->field);
  }
}
