<?php

/**
 * Implementation of hook_views_handlers().
 */
function views_taxonomy_hierarchy_top_filter_views_handlers() {
  return array(
    'handlers' => array(
      'views_taxonomy_hierarchy_top_filter_handler_field' => array(
        'parent' => 'views_handler_field_boolean',
      ),
      'views_taxonomy_hierarchy_top_filter_handler_filter' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      ),
      'views_taxonomy_hierarchy_top_filter_handler_sort' => array(
        'parent' => 'views_handler_sort',
      ),
    )
  );
}

/**
 * Implementaion of hook_views_data_alter()
 */
function views_taxonomy_hierarchy_top_filter_views_data_alter(&$data) {
  $data['term_hierarchy']['top_level_term'] = array(
    'title' => t('Top level term'),
    'help' => t('Whether or not the term is a top level taxonomy term.'),
    'real field' => 'parent',
    'field' => array(
      'handler' => 'views_taxonomy_hierarchy_top_filter_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_taxonomy_hierarchy_top_filter_handler_filter',
      'label' => t('Top Level'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_taxonomy_hierarchy_top_filter_handler_sort',
    ),
  );
}
