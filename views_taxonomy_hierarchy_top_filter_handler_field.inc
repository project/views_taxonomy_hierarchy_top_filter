<?php
class views_taxonomy_hierarchy_top_filter_handler_field extends views_handler_field_boolean {
  function render($values) {
    $value = $values->{$this->field_alias};
    if (!empty($this->options['not'])) {
      $value = !$value;
    }

    if (isset($this->formats[$this->options['type']])) {
      return $value ? $this->formats[$this->options['type']][1] : $this->formats[$this->options['type']][0];
    }
    else {
      return $value ? $this->formats['yes-no'][1] : $this->formats['yes-no'][0];
    }
  }
}
